(function() {
  'use strict';
  const wrapper = document.querySelector('[data-js="board-wrapper"]');
  const board = new ACBoard(10, 10, wrapper);

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function Car(board) {
    this.src = 'https://image.flaticon.com/icons/png/128/171/171239.png'
    this.board = board;
    this.stoped = false;
    this.addedToBoard = false;
  }

  Car.prototype.addToBoard = function(x,y) {
    if(this.addedToboard)
      return;

    this.x = x;
    this.y = y;
    this.board.addItem(x,y,this);
    this.addedToboard = true;
  }

  Car.prototype.start = function() {
    let newX = this.x + getRandomInt(-1, 2);
    let newY = this.y + getRandomInt(-1, 2);

    if(newX >= this.board.width || newX < 0)
      newX = this.x;

    if(newY >= this.board.width || newY < 0)
      newY = this.y;

    this.board.moveItem(this.x,this.y,newX, newY);
    this.x = newX;
    this.y = newY;

    if(!this.stoped)
      setTimeout(this.start.bind(this), 1000);

  }

var c = new Car(board);
c.addToBoard(1,1);
c.start();
// setTimeout(() => {
//   c.stoped = true;
// }, 3000)
}());
