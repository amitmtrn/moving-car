(function(global) {

  ACBoard.count = 0;

  function createBoard(ref) {
    ref.wrapper.append(ref.table);
    for (let i = 0; i < ref.width; i++) { // create rows
       const row = document.createElement('tr');
       ref.displayBoard[i] = [];
       ref.itemsBoard[i] = [];

       ref.table.append(row);
      for (let j = 0; j < ref.height; j++) { // create columns
        ref.displayBoard[i][j] = document.createElement('td');
        row.append(ref.displayBoard[i][j]);
      }
    }
  }


  function ACBoard(width = 5, height = 5, el = document.body) {
    if(ACBoard.count > 1)
      console.warn('you already have one board!');

    ACBoard.count++;

    this.width = width;
    this.height = height;
    this.wrapper = el;
    this.table = document.createElement('table');
    this.table.className = 'ac-board-' + ACBoard.count;
    this.displayBoard = [];
    this.itemsBoard = [];

    createBoard(this);
  }

  ACBoard.prototype.addItem = function(x, y, item) {
    if(x > this.width || y > this.height || x < 0 || y < 0)
      throw new Error('out of bonds');

    const img = document.createElement('img');
    img.src = item.src;

    if(item.src) {
      this.displayBoard[x][y].append(img);
      this.itemsBoard[x][y] = item;
      return item;
    }

      throw new Error('must get src in the object!');
  }

  ACBoard.prototype.moveItem = function(x1, y1, x2, y2) {
    if(x1 > this.width ||
      y1 > this.height ||
      x2 > this.width  ||
      y2 > this.height || x1 < 0 || y1 < 0 || x2 < 0 || y2 < 0)
      throw new Error('out of bonds');

    if(this.displayBoard[x1][y1].children.length === 0)
      throw new Error('no items in this board cell');

    const item = this.displayBoard[x1][y1].children[0];

    if(item instanceof HTMLElement) {
       this.itemsBoard[x2][y2] = this.itemsBoard[x1][y1];
       this.displayBoard[x2][y2].append(item);
       return this.itemsBoard[x2][y2];
    }

      throw new Error('must get HTMLElement!');
  }

  ACBoard.prototype.getItem = function(x, y) {
    if(x > this.width || y > this.height || x < 0 || y < 0)
      throw new Error('out of bonds');

    return this.itemsBoard[x][y];
  }

  ACBoard.prototype.removeItem = function(x, y) {
    this.displayBoard[x][y].innerHTML = '';
    delete this.itemsBoard[x][y];
  }


  global.ACBoard = ACBoard;

}(window));
